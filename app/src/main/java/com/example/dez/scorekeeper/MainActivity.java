package com.example.dez.scorekeeper;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    int scoreFighterA = 0;
    int scoreFighterB = 0;
    int totalScoreA = 0;
    int totalScoreB = 0;
    int roundNumber = 0;

    public void tenPointsForA(View v) {
        scoreFighterA = 10;
        displayFighterA(scoreFighterA);
    }

    public void ninePointsForA(View v) {
        scoreFighterA = 9;
        displayFighterA(scoreFighterA);
    }

    public void eightPointsForA(View v) {
        scoreFighterA = 8;
        displayFighterA(scoreFighterA);
    }

    public void sevenPointsForA(View v) {
        scoreFighterA = 7;
        displayFighterA(scoreFighterA);
    }

    public void tenPointsForB(View v) {
        scoreFighterB = 10;
        displayFighterB(scoreFighterB);
    }

    public void ninePointsForB(View v) {
        scoreFighterB = 9;
        displayFighterB(scoreFighterB);
    }

    public void eightPointsForB(View v) {
        scoreFighterB = 8;
        displayFighterB(scoreFighterB);
    }

    public void sevenPointsForB(View v) {
        scoreFighterB = 7;
        displayFighterB(scoreFighterB);
    }

    public void nextRound(View v) {
        if(roundNumber < 5) {
            totalScoreA = totalScoreA + scoreFighterA;
            totalScoreB = totalScoreB + scoreFighterB;
            scoreFighterA = 0;
            scoreFighterB = 0;
            roundNumber += 1;
            displayRoundNumber(roundNumber);
            displayFighterA(scoreFighterA);
            displayFighterB(scoreFighterB);
            displayTotalFighterA(totalScoreA);
            displayTotalFighterB(totalScoreB);
        }
    }

    public void resetAll(View v) {
        roundNumber = 0;
        scoreFighterA = 0;
        scoreFighterB = 0;
        totalScoreA = 0;
        totalScoreB = 0;
        displayFighterA(scoreFighterA);
        displayFighterB(scoreFighterB);
        displayTotalFighterA(totalScoreA);
        displayTotalFighterB(totalScoreB);
        displayRoundNumber(roundNumber);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void displayFighterA(int score) {
        TextView scoreView = (TextView) findViewById(R.id.fighter_a_score);
        scoreView.setText(String.valueOf(score));
    }

    public void displayFighterB(int score) {
        TextView scoreView = (TextView) findViewById(R.id.fighter_b_score);
        scoreView.setText(String.valueOf(score));
    }

    public void displayRoundNumber(int num) {
        TextView numView = (TextView) findViewById(R.id.round_number);
        numView.setText("Round Number: " + String.valueOf(num));
    }

    public void displayTotalFighterA(int score) {
        TextView scoreView = (TextView) findViewById(R.id.total_fighter_a_score);
        scoreView.setText("Total score: " + String.valueOf(score));
    }

    public void displayTotalFighterB(int score) {
        TextView scoreView = (TextView) findViewById(R.id.total_fighter_b_score);
        scoreView.setText("Total score: " + String.valueOf(score));
    }
}
